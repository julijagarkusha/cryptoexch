import "./styles/index.scss";

const bodyElement = document.querySelector('body');
const buttonGroup = document.querySelectorAll('.button__group');
const selectedElements = document.querySelectorAll('.select--selected');
const selectListElements = document.querySelectorAll(".select__list");
const selectOptionElements = document.querySelectorAll(".select__list li");
const selectSendElement = document.querySelector('#sendValue .select--selected');
const selectGetElement = document.querySelector('#getValue .select--selected');
const mobileMenuButton = document.querySelector('.menu--mobile');
const menuList = document.querySelector('.menu');

const buttonGroupClick = (event) => {
    event.preventDefault();
    const target = event.target;
    const getUnit = target.getAttribute('data-get');
    const sendUnit = target.getAttribute('data-send');
    const sendValue = target.getAttribute('data-send-value');
    const getValue = target.getAttribute('data-get-value');
    selectSendElement.setAttribute('data-unit', sendUnit);
    selectSendElement.setAttribute('data-value', sendValue);
    selectSendElement.innerHTML = sendValue;
    selectGetElement.setAttribute('data-unit', getUnit);
    selectGetElement.setAttribute('data-value', getValue);
    selectGetElement.innerHTML = getValue;
};

const optionsShow = (event) => {
  const target = event.target;
  const targetParent = target.parentNode;
  const listElements = targetParent.querySelector('.select__list');
  if(listElements.classList.contains("select__list--show")) {
      listElements.classList.add("select__list--hidden");
      setTimeout(() => {
          listElements.classList.remove("select__list--hidden");
          listElements.classList.remove("select__list--show");
      }, 300)
  } else {
      selectListElements.forEach(selectListElement => {
          selectListElement.classList.remove("select__list--show");
      });
      listElements.classList.add("select__list--show")
  }
};

const optionsHidden = (event) => {
  const target = event.target;
  if (!target.classList.contains('select--selected')) {
      selectListElements.forEach(selectListElement => {
          selectListElement.classList.remove("select__list--show");
      });
  }
};

const optionSelected = (event) => {
    const target = event.target;
    const targetParent = target.parentNode.parentNode;
    const selectedElement = targetParent.querySelector(".select--selected");
    const unitValue = target.getAttribute('data-unit');
    const unitText = target.getAttribute('data-value');
    selectedElement.setAttribute('data-unit', unitValue);
    selectedElement.setAttribute('data-value', unitText);
    selectedElement.innerHTML = unitText;
};

const mobileMenuToggle = (event) => {
    const target = event.target;
    if(target.classList.contains('menu--mobileShow')) {
        target.classList.add('menu--hidden');
        menuList.classList.add('menu--hidden');
        setTimeout(() => {
            target.classList.remove('menu--hidden');
            menuList.classList.remove('menu--hidden');
            target.classList.remove('menu--mobileShow');
            menuList.classList.remove('menu--show');
        }, 500);
    } else {
        target.classList.add('menu--mobileShow');
        menuList.classList.add('menu--show');
    }
};

buttonGroup.forEach(buttonsItem => {
    buttonsItem.addEventListener('click', buttonGroupClick)
});

selectedElements.forEach(selectedElement => {
    selectedElement.addEventListener('click', optionsShow)
});

bodyElement.addEventListener('click', optionsHidden);

selectOptionElements.forEach(selectOptionElement => {
   selectOptionElement.addEventListener('click', optionSelected)
});

mobileMenuButton.addEventListener('click', mobileMenuToggle);